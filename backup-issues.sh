#!/bin/sh
warcfile="r7rs-issues-$(date +%Y%m%d)"

wget --mirror -np \
     --wait 2 \
     --warc-file "$warcfile" \
     --reject-regex='https://codeberg.org/scheme/r7rs/([^ip]|i[^s]|is[^s]|iss[^u]|issu[^e]|issue[^s]|issues\?[^p]|issues\?p[^a]|issues\?pa[^g]|issues\?pag[^e]|issues\?page[^=]|p[^u]|pu[^l]|pul[^l]|pull[^s]|pulls\?[^p]|pulls\?p[^a]|pulls\?pa[^g]|pulls\?pag[^e]|pulls\?page[^=])' \
     https://codeberg.org/scheme/r7rs/issues \
     https://codeberg.org/scheme/r7rs/pulls

# TODO: automatically upload the backup file to the Internet Archive
# using the ia command line tool
# <https://archive.org/developers/internetarchive/>
