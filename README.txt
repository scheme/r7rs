R7RS-LARGE REPORT

Vincent Manis <vmanis AT telus DOT net> 2022-12-23

This directory contains the TeX sources and build scripts for the
R7RS-Large Report. At present, it is envisioned that the Report will
consist of several separate documents (Foundations, Batteries
Included, Environment, and perhaps others). Each of these documents
will be added as a separate subdirectory here.

DEPENDENCIES

As its name suggests, build.bash requires a bash shell. If you are on
a Mac, the bash supplied with OSX is ancient. If the script doesn't
work, I would suggest installing the version of bash provided with
HomeBrew. On Windows, WSL2 is suggested.

You will need at least a minimal TeX system to format the
document. There is no need to install a complete TeX system; for
example, on a Debian (Ubuntu, Mint, etc) system, the package
texlive-base should be enough (later you will need xindy and
xindy-rules). 

You will also need a running Common Lisp or Scheme system to run
TeX2Page; the version here has been tested on SBCL, though it has been
reported that it also runs fine on Racket.

TeX2page generates PNG files, and therefore you will need NetPBM installed.

Thanks to Dorai Sitaram for his work on updating TeX2Page to improve 
its behavior on R7RS.

MANIFEST

This directory consists of the following.
  * build.bash: a script that builds one document, leaving behind a
    PDF file and an HTML directory
  * tex2page: a copy of the current TeX2page distribution (see
    PROVENANCE below)
  * r7rs-t2p-macros: the most current version of the macro set to be
    used with the R7RS document
  * r7rs-small: the TeX source for R7RS-Small
The TeX2page distribution is included so as to simplify the
installation process.

PROVENANCE

  * tex2page was obtained from  https://github.com/ds26gte/tex2page.git.
  * r7rs-t2p-macros was obtained from https://gitlab.com/ds26gte/r7rs-t2p-macros.
  * r7rs-small was obtained from https://github.com/johnwcowan/r7rs-spec.git
    (the `errata' branch); the subdirectory spec was renamed to r7rs.

HOW TO USE THE BUILD SCRIPT

First, if you don't have them, install TeX and a Common Lisp or Scheme
system (TeX2Page runs on a wide variety of Lisps and Schemes). Then install 
TeX2page; the INSTALL.adoc file will explain how. I leave TeX2page in this
directory, and copy the tex2page script to a directory on my path.

To process a document:

  1. If it doesn't exist, create a subdirectory named release; the PDF
     and HTML files will go there. 
  2. Issue the command
       ./build.bash --setup r7rs-small r7rs
  3. In the relatively unlikely case that either TeX or TeX2Page
     reports that a further pass or two is necessary, do
       ./build.bash r7rs-small r7rs
  4. The resulting files will be found in the subdirectory release.

BUGS AND TODOS

There are lots. These are roughly arranged in decreasing priority.

  1. Still lots of cosmetic problems in the HTML output; in some
     cases, we might want to fiddle the t2p file, in other cases, the 
     LaTeX source for the document can be adjusted.
  2. Move the class and style files out of the r7rs-small directory
     into a formats subdirectory.
  3. Reduce or eliminate the large number of underfull boxes that TeX
     complains about. This will make the output very slightly tidier.
  4. Indexing seems to have been done by a Scheme program. There is no
     reason not to use the Xindy package, which should be more than
     adequate for this purpose. This should be doable by adjusting
     some of the macros. I'm not sure whether Xindy is provided by
     M{iK,ac}TeX, which may limit its use on non-Linux systems.
  5. If somebody wants to do it, a Windows Batch or PowerShell
     counterpart of build.bash would be nice.

I will (eventually) investigate the Mac issues; someone else might
look at running this thing on Windows.

------------------------ (end) ----------------------------------
