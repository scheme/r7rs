(!label "historysection" 2 "TAG:__tex2page_chap_Temp_3" "IGNORE")
(!label "semanticsection" 3 "TAG:__tex2page_sec_1.1" "1.1")
(!label "qualifiers" 3 "TAG:__tex2page_sec_1.3.1" "1.3.1")
(!label "errorsituations" 3 "TAG:__tex2page_sec_1.3.2" "1.3.2")
(!label "typeconventions" 3 "TAG:__tex2page_sec_1.3.3" "1.3.3")
(!label "syntaxsection" 4 "TAG:__tex2page_sec_2.1" "2.1")
(!label "wscommentsection" 4 "TAG:__tex2page_sec_2.2" "2.2")
(!label ";" 4 "TAG:__tex2page_sec_2.2" "2.2")
(!label "labelsection" 4 "TAG:__tex2page_sec_2.4" "2.4")
(!label "basicchapter" 5 "TAG:__tex2page_chap_3" "3")
(!label "specialformsection" 5 "TAG:__tex2page_sec_3.1" "3.1")
(!label "variablesection" 5 "TAG:__tex2page_sec_3.1" "3.1")
(!label "disjointness" 5 "TAG:__tex2page_sec_3.2" "3.2")
(!label "externalreps" 5 "TAG:__tex2page_sec_3.3" "3.3")
(!label "storagemodel" 5 "TAG:__tex2page_sec_3.4" "3.4")
(!label "proper tail recursion" 5 "TAG:__tex2page_sec_3.5" "3.5")
(!label "expressionchapter" 6 "TAG:__tex2page_chap_4" "4")
(!label "primitivexps" 6 "TAG:__tex2page_sec_4.1" "4.1")
(!label "literalsection" 6 "TAG:__tex2page_sec_4.1.2" "4.1.2")
(!label "quote" 6 "TAG:__tex2page_sec_4.1.2" "4.1.2")
(!label "'" 6 "TAG:__tex2page_sec_4.1.2" "4.1.2")
(!label "lamba" 6 "TAG:__tex2page_sec_4.1.4" "4.1.4")
(!label "lambda" 6 "TAG:__tex2page_sec_4.1.4" "4.1.4")
(!label "if" 6 "TAG:__tex2page_sec_4.1.5" "4.1.5")
(!label "if" 6 "TAG:__tex2page_sec_4.1.5" "4.1.5")
(!label "assignment" 6 "TAG:__tex2page_sec_4.1.6" "4.1.6")
(!label "set!" 6 "TAG:__tex2page_sec_4.1.6" "4.1.6")
(!label "inclusion" 6 "TAG:__tex2page_sec_4.1.7" "4.1.7")
(!label "include" 6 "TAG:__tex2page_sec_4.1.7" "4.1.7")
(!label "include-ci" 6 "TAG:__tex2page_sec_4.1.7" "4.1.7")
(!label "derivedexps" 6 "TAG:__tex2page_sec_4.2" "4.2")
(!label "cond" 6 "TAG:__tex2page_sec_4.2.1" "4.2.1")
(!label "else" 6 "TAG:__tex2page_sec_4.2.1" "4.2.1")
(!label "=>" 6 "TAG:__tex2page_sec_4.2.1" "4.2.1")
(!label "case" 6 "TAG:__tex2page_sec_4.2.1" "4.2.1")
(!label "and" 6 "TAG:__tex2page_sec_4.2.1" "4.2.1")
(!label "or" 6 "TAG:__tex2page_sec_4.2.1" "4.2.1")
(!label "when" 6 "TAG:__tex2page_sec_4.2.1" "4.2.1")
(!label "unless" 6 "TAG:__tex2page_sec_4.2.1" "4.2.1")
(!label "cond-expand" 6 "TAG:__tex2page_sec_4.2.1" "4.2.1")
(!label "bindingsection" 6 "TAG:__tex2page_sec_4.2.2" "4.2.2")
(!label "let" 6 "TAG:__tex2page_sec_4.2.2" "4.2.2")
(!label "let*" 6 "TAG:__tex2page_sec_4.2.2" "4.2.2")
(!label "letrec" 6 "TAG:__tex2page_sec_4.2.2" "4.2.2")
(!label "letrec*" 6 "TAG:__tex2page_sec_4.2.2" "4.2.2")
(!label "letrecstar" 6 "TAG:__tex2page_sec_4.2.2" "4.2.2")
(!label "let-values" 6 "TAG:__tex2page_sec_4.2.2" "4.2.2")
(!label "let*-values" 6 "TAG:__tex2page_sec_4.2.2" "4.2.2")
(!label "sequencing" 6 "TAG:__tex2page_sec_4.2.3" "4.2.3")
(!label "begin" 6 "TAG:__tex2page_sec_4.2.3" "4.2.3")
(!label "begin" 6 "TAG:__tex2page_sec_4.2.3" "4.2.3")
(!label "do" 6 "TAG:__tex2page_sec_4.2.4" "4.2.4")
(!label "let" 6 "TAG:__tex2page_sec_4.2.4" "4.2.4")
(!label "namedlet" 6 "TAG:__tex2page_sec_4.2.4" "4.2.4")
(!label "delay" 6 "TAG:__tex2page_sec_4.2.5" "4.2.5")
(!label "delay-force" 6 "TAG:__tex2page_sec_4.2.5" "4.2.5")
(!label "force" 6 "TAG:__tex2page_sec_4.2.5" "4.2.5")
(!label "promise?" 6 "TAG:__tex2page_sec_4.2.5" "4.2.5")
(!label "make-promise" 6 "TAG:__tex2page_sec_4.2.5" "4.2.5")
(!label "make-parameter" 6 "TAG:__tex2page_sec_4.2.6" "4.2.6")
(!label "make-parameter" 6 "TAG:__tex2page_sec_4.2.6" "4.2.6")
(!label "parameterize" 6 "TAG:__tex2page_sec_4.2.6" "4.2.6")
(!label "guard" 6 "TAG:__tex2page_sec_4.2.7" "4.2.7")
(!label "quasiquotesection" 6 "TAG:__tex2page_sec_4.2.8" "4.2.8")
(!label "quasiquote" 6 "TAG:__tex2page_sec_4.2.8" "4.2.8")
(!label "," 6 "TAG:__tex2page_sec_4.2.8" "4.2.8")
(!label ",@" 6 "TAG:__tex2page_sec_4.2.8" "4.2.8")
(!label "`" 6 "TAG:__tex2page_sec_4.2.8" "4.2.8")
(!label "caselambdasection" 6 "TAG:__tex2page_sec_4.2.9" "4.2.9")
(!label "case-lambda" 6 "TAG:__tex2page_sec_4.2.9" "4.2.9")
(!label "macrosection" 6 "TAG:__tex2page_sec_4.3" "4.3")
(!label "bindsyntax" 6 "TAG:__tex2page_sec_4.3.1" "4.3.1")
(!label "let-syntax" 6 "TAG:__tex2page_sec_4.3.1" "4.3.1")
(!label "letrec-syntax" 6 "TAG:__tex2page_sec_4.3.1" "4.3.1")
(!label "patternlanguage" 6 "TAG:__tex2page_sec_4.3.2" "4.3.2")
(!label "_" 6 "TAG:__tex2page_sec_4.3.2" "4.3.2")
(!label "syntax-error" 6 "TAG:__tex2page_sec_4.3.3" "4.3.3")
(!label "programchapter" 7 "TAG:__tex2page_chap_5" "5")
(!label "import" 7 "TAG:__tex2page_sec_5.2" "5.2")
(!label "defines" 7 "TAG:__tex2page_sec_5.3" "5.3")
(!label "define" 7 "TAG:__tex2page_sec_5.3" "5.3")
(!label "internaldefines" 7 "TAG:__tex2page_sec_5.3.2" "5.3.2")
(!label "define-values" 7 "TAG:__tex2page_sec_5.3.3" "5.3.3")
(!label "define-syntax" 7 "TAG:__tex2page_sec_5.4" "5.4")
(!label "usertypes" 7 "TAG:__tex2page_sec_5.5" "5.5")
(!label "define-record-type" 7 "TAG:__tex2page_sec_5.5" "5.5")
(!label "libraries" 7 "TAG:__tex2page_sec_5.6" "5.6")
(!label "define-library" 7 "TAG:__tex2page_sec_5.6.1" "5.6.1")
(!label "librarydeclarations" 7 "TAG:__tex2page_sec_5.6.1" "5.6.1")
(!label "initialenv" 8 "TAG:__tex2page_chap_6" "6")
(!label "builtinchapter" 8 "TAG:__tex2page_chap_6" "6")
(!label "equivalencesection" 8 "TAG:__tex2page_sec_6.1" "6.1")
(!label "eqv?" 8 "TAG:__tex2page_sec_6.1" "6.1")
(!label "eq?" 8 "TAG:__tex2page_sec_6.1" "6.1")
(!label "equal?" 8 "TAG:__tex2page_sec_6.1" "6.1")
(!label "numbersection" 8 "TAG:__tex2page_sec_6.2" "6.2")
(!label "numericaltypes" 8 "TAG:__tex2page_sec_6.2.1" "6.2.1")
(!label "exactly" 8 "TAG:__tex2page_sec_6.2.2" "6.2.2")
(!label "restrictions" 8 "TAG:__tex2page_sec_6.2.3" "6.2.3")
(!label "numbernotations" 8 "TAG:__tex2page_sec_6.2.5" "6.2.5")
(!label "number?" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "complex?" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "real?" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "rational?" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "integer?" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "exact?" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "inexact?" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "exact-integer?" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "finite?" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "infinite?" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "nan?" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "=" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "<" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label ">" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "<=" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label ">=" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "zero?" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "positive?" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "negative?" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "odd?" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "even?" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "max" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "min" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "+" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "*" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "-" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "-" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "/" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "/" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "abs" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "floor/" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "floor-quotient" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "floor-remainder" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "truncate/" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "truncate-quotient" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "truncate-remainder" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "quotient" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "remainder" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "modulo" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "gcd" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "lcm" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "numerator" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "denominator" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "floor" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "ceiling" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "truncate" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "round" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "rationalize" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "exp" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "log" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "log" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "sin" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "cos" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "tan" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "asin" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "acos" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "atan" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "atan" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "square" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "sqrt" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "exact-integer-sqrt" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "expt" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "make-rectangular" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "make-polar" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "real-part" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "imag-part" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "magnitude" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "angle" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "inexact" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "exact" 8 "TAG:__tex2page_sec_6.2.6" "6.2.6")
(!label "number->string" 8 "TAG:__tex2page_sec_6.2.7" "6.2.7")
(!label "number->string" 8 "TAG:__tex2page_sec_6.2.7" "6.2.7")
(!label "string->number" 8 "TAG:__tex2page_sec_6.2.7" "6.2.7")
(!label "string->number" 8 "TAG:__tex2page_sec_6.2.7" "6.2.7")
(!label "booleansection" 8 "TAG:__tex2page_sec_6.3" "6.3")
(!label "not" 8 "TAG:__tex2page_sec_6.3" "6.3")
(!label "boolean?" 8 "TAG:__tex2page_sec_6.3" "6.3")
(!label "boolean=?" 8 "TAG:__tex2page_sec_6.3" "6.3")
(!label "listsection" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "pair?" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "cons" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "car" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "cdr" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "set-car!" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "set-cdr!" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "caar" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "cadr" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "cdar" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "cddr" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "caaar" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "caadr" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "cadar" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "caddr" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "cdaar" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "cdadr" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "cddar" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "cdddr" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "caaaar" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "caaadr" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "caadar" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "caaddr" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "cadaar" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "cadadr" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "caddar" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "cadddr" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "cdaaar" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "cdaadr" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "cdadar" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "cdaddr" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "cddaar" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "cddadr" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "cdddar" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "cddddr" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "null?" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "list?" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "make-list" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "make-list" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "list" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "length" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "append" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "reverse" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "list-tail" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "list-ref" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "list-set!" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "memq" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "memv" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "member" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "member" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "assq" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "assv" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "assoc" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "assoc" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "list-copy" 8 "TAG:__tex2page_sec_6.4" "6.4")
(!label "symbolsection" 8 "TAG:__tex2page_sec_6.5" "6.5")
(!label "symbol?" 8 "TAG:__tex2page_sec_6.5" "6.5")
(!label "symbol=?" 8 "TAG:__tex2page_sec_6.5" "6.5")
(!label "symbol->string" 8 "TAG:__tex2page_sec_6.5" "6.5")
(!label "string->symbol" 8 "TAG:__tex2page_sec_6.5" "6.5")
(!label "charactersection" 8 "TAG:__tex2page_sec_6.6" "6.6")
(!label "char?" 8 "TAG:__tex2page_sec_6.6" "6.6")
(!label "char=?" 8 "TAG:__tex2page_sec_6.6" "6.6")
(!label "char<?" 8 "TAG:__tex2page_sec_6.6" "6.6")
(!label "char>?" 8 "TAG:__tex2page_sec_6.6" "6.6")
(!label "char<=?" 8 "TAG:__tex2page_sec_6.6" "6.6")
(!label "char>=?" 8 "TAG:__tex2page_sec_6.6" "6.6")
(!label "characterequality" 8 "TAG:__tex2page_sec_6.6" "6.6")
(!label "char-ci=?" 8 "TAG:__tex2page_sec_6.6" "6.6")
(!label "char-ci<?" 8 "TAG:__tex2page_sec_6.6" "6.6")
(!label "char-ci>?" 8 "TAG:__tex2page_sec_6.6" "6.6")
(!label "char-ci<=?" 8 "TAG:__tex2page_sec_6.6" "6.6")
(!label "char-ci>=?" 8 "TAG:__tex2page_sec_6.6" "6.6")
(!label "char-alphabetic?" 8 "TAG:__tex2page_sec_6.6" "6.6")
(!label "char-numeric?" 8 "TAG:__tex2page_sec_6.6" "6.6")
(!label "char-whitespace?" 8 "TAG:__tex2page_sec_6.6" "6.6")
(!label "char-upper-case?" 8 "TAG:__tex2page_sec_6.6" "6.6")
(!label "char-lower-case?" 8 "TAG:__tex2page_sec_6.6" "6.6")
(!label "digit-value" 8 "TAG:__tex2page_sec_6.6" "6.6")
(!label "char->integer" 8 "TAG:__tex2page_sec_6.6" "6.6")
(!label "integer->char" 8 "TAG:__tex2page_sec_6.6" "6.6")
(!label "char-upcase" 8 "TAG:__tex2page_sec_6.6" "6.6")
(!label "char-downcase" 8 "TAG:__tex2page_sec_6.6" "6.6")
(!label "char-foldcase" 8 "TAG:__tex2page_sec_6.6" "6.6")
(!label "stringsection" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string?" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "make-string" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "make-string" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string-length" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string-ref" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string-set!" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string=?" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string-ci=?" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string<?" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string-ci<?" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string>?" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string-ci>?" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string<=?" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string-ci<=?" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string>=?" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string-ci>=?" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string-upcase" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string-downcase" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string-foldcase" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "substring" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string-append" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string->list" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string->list" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string->list" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "list->string" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string-copy" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string-copy" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string-copy" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string-copy!" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string-copy!" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string-copy!" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string-fill!" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string-fill!" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "string-fill!" 8 "TAG:__tex2page_sec_6.7" "6.7")
(!label "vectorsection" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "vector?" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "make-vector" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "make-vector" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "vector" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "vector-length" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "vector-ref" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "vector-set!" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "vector->list" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "vector->list" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "vector->list" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "list->vector" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "vector->string" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "vector->string" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "vector->string" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "string->vector" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "string->vector" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "string->vector" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "vectortostring" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "vector-copy" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "vector-copy" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "vector-copy" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "vector-copy!" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "vector-copy!" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "vector-copy!" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "vector-append" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "vector-fill!" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "vector-fill!" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "vector-fill!" 8 "TAG:__tex2page_sec_6.8" "6.8")
(!label "bytevectorsection" 8 "TAG:__tex2page_sec_6.9" "6.9")
(!label "bytevector?" 8 "TAG:__tex2page_sec_6.9" "6.9")
(!label "make-bytevector" 8 "TAG:__tex2page_sec_6.9" "6.9")
(!label "make-bytevector" 8 "TAG:__tex2page_sec_6.9" "6.9")
(!label "bytevector" 8 "TAG:__tex2page_sec_6.9" "6.9")
(!label "bytevector-length" 8 "TAG:__tex2page_sec_6.9" "6.9")
(!label "bytevector-u8-ref" 8 "TAG:__tex2page_sec_6.9" "6.9")
(!label "bytevector-u8-set!" 8 "TAG:__tex2page_sec_6.9" "6.9")
(!label "bytevector-copy" 8 "TAG:__tex2page_sec_6.9" "6.9")
(!label "bytevector-copy" 8 "TAG:__tex2page_sec_6.9" "6.9")
(!label "bytevector-copy" 8 "TAG:__tex2page_sec_6.9" "6.9")
(!label "bytevector-copy!" 8 "TAG:__tex2page_sec_6.9" "6.9")
(!label "bytevector-copy!" 8 "TAG:__tex2page_sec_6.9" "6.9")
(!label "bytevector-copy!" 8 "TAG:__tex2page_sec_6.9" "6.9")
(!label "bytevector-append" 8 "TAG:__tex2page_sec_6.9" "6.9")
(!label "utf8tostring" 8 "TAG:__tex2page_sec_6.9" "6.9")
(!label "utf8->string" 8 "TAG:__tex2page_sec_6.9" "6.9")
(!label "utf8->string" 8 "TAG:__tex2page_sec_6.9" "6.9")
(!label "utf8->string" 8 "TAG:__tex2page_sec_6.9" "6.9")
(!label "string->utf8" 8 "TAG:__tex2page_sec_6.9" "6.9")
(!label "string->utf8" 8 "TAG:__tex2page_sec_6.9" "6.9")
(!label "string->utf8" 8 "TAG:__tex2page_sec_6.9" "6.9")
(!label "proceduresection" 8 "TAG:__tex2page_sec_6.10" "6.10")
(!label "procedure?" 8 "TAG:__tex2page_sec_6.10" "6.10")
(!label "apply" 8 "TAG:__tex2page_sec_6.10" "6.10")
(!label "map" 8 "TAG:__tex2page_sec_6.10" "6.10")
(!label "string-map" 8 "TAG:__tex2page_sec_6.10" "6.10")
(!label "stringmap" 8 "TAG:__tex2page_sec_6.10" "6.10")
(!label "vector-map" 8 "TAG:__tex2page_sec_6.10" "6.10")
(!label "for-each" 8 "TAG:__tex2page_sec_6.10" "6.10")
(!label "string-for-each" 8 "TAG:__tex2page_sec_6.10" "6.10")
(!label "vector-for-each" 8 "TAG:__tex2page_sec_6.10" "6.10")
(!label "call-with-current-continuation" 8 "TAG:__tex2page_sec_6.10" "6.10")
(!label "call/cc" 8 "TAG:__tex2page_sec_6.10" "6.10")
(!label "continuations" 8 "TAG:__tex2page_sec_6.10" "6.10")
(!label "values" 8 "TAG:__tex2page_sec_6.10" "6.10")
(!label "call-with-values" 8 "TAG:__tex2page_sec_6.10" "6.10")
(!label "dynamic-wind" 8 "TAG:__tex2page_sec_6.10" "6.10")
(!label "exceptionsection" 8 "TAG:__tex2page_sec_6.11" "6.11")
(!label "with-exception-handler" 8 "TAG:__tex2page_sec_6.11" "6.11")
(!label "raise" 8 "TAG:__tex2page_sec_6.11" "6.11")
(!label "raise-continuable" 8 "TAG:__tex2page_sec_6.11" "6.11")
(!label "error" 8 "TAG:__tex2page_sec_6.11" "6.11")
(!label "error-object?" 8 "TAG:__tex2page_sec_6.11" "6.11")
(!label "error-object-message" 8 "TAG:__tex2page_sec_6.11" "6.11")
(!label "error-object-irritants" 8 "TAG:__tex2page_sec_6.11" "6.11")
(!label "read-error?" 8 "TAG:__tex2page_sec_6.11" "6.11")
(!label "file-error?" 8 "TAG:__tex2page_sec_6.11" "6.11")
(!label "environment" 8 "TAG:__tex2page_sec_6.12" "6.12")
(!label "environments" 8 "TAG:__tex2page_sec_6.12" "6.12")
(!label "scheme-report-environment" 8 "TAG:__tex2page_sec_6.12" "6.12")
(!label "null-environment" 8 "TAG:__tex2page_sec_6.12" "6.12")
(!label "interaction-environment" 8 "TAG:__tex2page_sec_6.12" "6.12")
(!label "eval" 8 "TAG:__tex2page_sec_6.12" "6.12")
(!label "portsection" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "call-with-port" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "call-with-input-file" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "call-with-output-file" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "input-port?" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "output-port?" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "textual-port?" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "binary-port?" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "port?" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "input-port-open?" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "output-port-open?" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "current-input-port" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "current-output-port" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "current-error-port" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "with-input-from-file" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "with-output-to-file" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "open-input-file" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "open-binary-input-file" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "open-output-file" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "open-binary-output-file" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "close-port" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "close-input-port" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "close-output-port" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "open-input-string" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "open-output-string" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "get-output-string" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "open-input-bytevector" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "open-output-bytevector" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "get-output-bytevector" 8 "TAG:__tex2page_sec_6.13.1" "6.13.1")
(!label "inputsection" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "read" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "read" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "read-char" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "read-char" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "peek-char" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "peek-char" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "read-line" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "read-line" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "eof-object?" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "eof-object" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "char-ready?" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "char-ready?" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "read-string" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "read-string" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "readstring" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "read-u8" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "read-u8" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "peek-u8" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "peek-u8" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "u8-ready?" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "u8-ready?" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "read-bytevector" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "read-bytevector" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "read-bytevector!" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "read-bytevector!" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "read-bytevector!" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "read-bytevector!" 8 "TAG:__tex2page_sec_6.13.2" "6.13.2")
(!label "outputsection" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "write" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "write" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "write-shared" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "write-shared" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "write-simple" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "write-simple" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "display" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "display" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "newline" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "newline" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "write-char" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "write-char" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "write-string" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "write-string" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "write-string" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "write-string" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "write-u8" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "write-u8" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "write-bytevector" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "write-bytevector" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "write-bytevector" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "write-bytevector" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "flush-output-port" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "flush-output-port" 8 "TAG:__tex2page_sec_6.13.3" "6.13.3")
(!label "load" 8 "TAG:__tex2page_sec_6.14" "6.14")
(!label "load" 8 "TAG:__tex2page_sec_6.14" "6.14")
(!label "file-exists?" 8 "TAG:__tex2page_sec_6.14" "6.14")
(!label "delete-file" 8 "TAG:__tex2page_sec_6.14" "6.14")
(!label "command-line" 8 "TAG:__tex2page_sec_6.14" "6.14")
(!label "exit" 8 "TAG:__tex2page_sec_6.14" "6.14")
(!label "exit" 8 "TAG:__tex2page_sec_6.14" "6.14")
(!label "emergency-exit" 8 "TAG:__tex2page_sec_6.14" "6.14")
(!label "emergency-exit" 8 "TAG:__tex2page_sec_6.14" "6.14")
(!label "get-environment-variable" 8 "TAG:__tex2page_sec_6.14" "6.14")
(!label "get-environment-variables" 8 "TAG:__tex2page_sec_6.14" "6.14")
(!label "current-second" 8 "TAG:__tex2page_sec_6.14" "6.14")
(!label "current-jiffy" 8 "TAG:__tex2page_sec_6.14" "6.14")
(!label "jiffies-per-second" 8 "TAG:__tex2page_sec_6.14" "6.14")
(!label "features" 8 "TAG:__tex2page_sec_6.14" "6.14")
(!label "formalchapter" 9 "TAG:__tex2page_chap_7" "7")
(!label "BNF" 9 "TAG:__tex2page_sec_7.1" "7.1")
(!label "extendedalphas" 9 "TAG:__tex2page_sec_7.1.1" "7.1.1")
(!label "identifiersyntax" 9 "TAG:__tex2page_sec_7.1.1" "7.1.1")
(!label "charactersyntax" 9 "TAG:__tex2page_sec_7.1.1" "7.1.1")
(!label "numbersyntax" 9 "TAG:__tex2page_sec_7.1.1" "7.1.1")
(!label "datumsyntax" 9 "TAG:__tex2page_sec_7.1.2" "7.1.2")
(!label "datum" 9 "TAG:__tex2page_sec_7.1.2" "7.1.2")
(!label "formalsemanticssection" 9 "TAG:__tex2page_sec_7.2" "7.2")
(!label "derivedsection" 9 "TAG:__tex2page_sec_7.3" "7.3")
(!label "stdlibraries" 10 "TAG:__tex2page_chap_A" "A")
(!label "stdfeatures" 11 "TAG:__tex2page_chap_B" "B")
(!label "standard_features" 11 "TAG:__tex2page_chap_B" "B")
(!label "incompatibilities" 12 "TAG:__tex2page_sec_Temp_7" "IGNORE")
(!label "differences" 12 "TAG:__tex2page_sec_Temp_8" "IGNORE")
(!label "cite{SICP}" 15 "TAG:__tex2page_bib_1" "1")
(!label "cite{Bawden88}" 15 "TAG:__tex2page_bib_2" "2")
(!label "cite{rfc2119}" 15 "TAG:__tex2page_bib_3" "3")
(!label "cite{howtoprint}" 15 "TAG:__tex2page_bib_4" "4")
(!label "cite{howtoread}" 15 "TAG:__tex2page_bib_5" "5")
(!label "cite{propertailrecursion}" 15 "TAG:__tex2page_bib_6" "6")
(!label "cite{srfi6}" 15 "TAG:__tex2page_bib_7" "7")
(!label "cite{RRRS}" 15 "TAG:__tex2page_bib_8" "8")
(!label "cite{macrosthatwork}" 15 "TAG:__tex2page_bib_9" "9")
(!label "cite{R4RS}" 15 "TAG:__tex2page_bib_10" "10")
(!label "cite{uax44}" 15 "TAG:__tex2page_bib_11" "11")
(!label "cite{syntacticabstraction}" 15 "TAG:__tex2page_bib_12" "12")
(!label "cite{srfi4}" 15 "TAG:__tex2page_bib_13" "13")
(!label "cite{Scheme311}" 15 "TAG:__tex2page_bib_14" "14")
(!label "cite{Scheme84}" 15 "TAG:__tex2page_bib_15" "15")
(!label "cite{life}" 15 "TAG:__tex2page_bib_16" "16")
(!label "cite{IEEE}" 15 "TAG:__tex2page_bib_17" "17")
(!label "cite{IEEEScheme}" 15 "TAG:__tex2page_bib_18" "18")
(!label "cite{srfi9}" 15 "TAG:__tex2page_bib_19" "19")
(!label "cite{R5RS}" 15 "TAG:__tex2page_bib_20" "20")
(!label "cite{Kohlbecker86}" 15 "TAG:__tex2page_bib_21" "21")
(!label "cite{hygienic}" 15 "TAG:__tex2page_bib_22" "22")
(!label "cite{McCarthy}" 15 "TAG:__tex2page_bib_23" "23")
(!label "cite{MITScheme}" 15 "TAG:__tex2page_bib_24" "24")
(!label "cite{Naur63}" 15 "TAG:__tex2page_bib_25" "25")
(!label "cite{Penfield81}" 15 "TAG:__tex2page_bib_26" "26")
(!label "cite{Rees82}" 15 "TAG:__tex2page_bib_27" "27")
(!label "cite{Rees84}" 15 "TAG:__tex2page_bib_28" "28")
(!label "cite{R3RS}" 15 "TAG:__tex2page_bib_29" "29")
(!label "cite{srfi1}" 15 "TAG:__tex2page_bib_30" "30")
(!label "cite{Scheme78}" 15 "TAG:__tex2page_bib_31" "31")
(!label "cite{Rabbit}" 15 "TAG:__tex2page_bib_32" "32")
(!label "cite{R6RS}" 15 "TAG:__tex2page_bib_33" "33")
(!label "cite{CLtL}" 15 "TAG:__tex2page_bib_34" "34")
(!label "cite{Scheme75}" 15 "TAG:__tex2page_bib_35" "35")
(!label "cite{Stoy77}" 15 "TAG:__tex2page_bib_36" "36")
(!label "cite{TImanual85}" 15 "TAG:__tex2page_bib_37" "37")
(!label "cite{srfi45}" 15 "TAG:__tex2page_bib_38" "38")
(!label "cite{GasbichlerKnauelSperberKelsey2003}" 15 "TAG:__tex2page_bib_39"
        "39")
(!label "cite{TAI}" 15 "TAG:__tex2page_bib_40" "40")
