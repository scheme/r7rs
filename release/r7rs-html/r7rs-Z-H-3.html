<!DOCTYPE html>
<html lang=en>
<!--
Generated from r7rs.tex by tex2page, v. 20221219
Copyright (C) 1997-2022 Dorai Sitaram
(running on SBCL 2.2.11 Linux)
http://ds26gte.github.io/tex2page/index.html
-->
<head>
<meta charset="utf-8">
<title>
Revised^7 Report on the Algorithmic Language Scheme
</title>
<link rel="stylesheet" href="r7rs-Z-S.css" />
<meta name=robots content="index,follow">
</head>
<body>
<div>
<div class=navigation>[Go to <span><a class=hrefinternal href="r7rs.html">first</a>, <a class=hrefinternal href="r7rs-Z-H-2.html">previous</a></span><span>, <a class=hrefinternal href="r7rs-Z-H-4.html">next</a></span> page<span>; &#xa0;&#xa0;</span><span><a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc">contents</a></span><span><span>; &#xa0;&#xa0;</span><a class=hrefinternal href="r7rs.html#TAG:__tex2page_index_start">index</a></span>]</div>
<p>
</p>
<a id="TAG:__tex2page_chap_1"></a>
<h1 class=chapter>
<div class=chapterheading><a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc_TAG:__tex2page_chap_1">Chapter 1</a></div><br>
<a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc_TAG:__tex2page_chap_1">Overview of Scheme</a></h1>
<p class=noindent></p>
<a id="TAG:__tex2page_sec_1.1"></a>
<h2 class=section><a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc_TAG:__tex2page_sec_1.1">1.1&#xa0;&#xa0;Semantics</a></h2>
<p class=noindent></p>
<p>

This section gives an overview of Scheme&#x2019;s semantics.  A
detailed informal semantics is the subject of
chapters&#xa0;<a class=hrefinternal href="r7rs-Z-H-5.html#TAG:__tex2page_chap_3">3</a> through&#xa0;<a class=hrefinternal href="r7rs-Z-H-8.html#TAG:__tex2page_chap_6">6</a>.  For reference
purposes, section&#xa0;<a class=hrefinternal href="r7rs-Z-H-9.html#TAG:__tex2page_sec_7.2">7.2</a> provides a formal
semantics of Scheme.</p>
<p>

Scheme is a statically scoped programming
language.  After macros are expanded, each use of a variable is associated
with a lexically apparent binding of that variable.</p>
<p>

Scheme is a dynamically typed language.  Types
are associated with values (also called objects<a id="TAG:__tex2page_index_2"></a>) rather than
with variables.  
Statically typed languages, by contrast, associate types with
variables and expressions as well as with values.</p>
<p>

All objects created in the course of a Scheme computation, including
procedures and continuations, have unlimited extent.
No Scheme object is ever destroyed.  The reason that
implementations of Scheme do not (usually!)&#x200b; &#x200b;run out of storage is that
they are permitted to reclaim the storage occupied by an object if
they can prove that the object cannot possibly matter to any future
computation.
Implementations of Scheme are required to be properly tail-recursive.
This allows the execution of an iterative computation in constant space,
even if the iterative computation is described by a syntactically
recursive procedure.  Thus with a properly tail-recursive implementation,
iteration can be expressed using the ordinary procedure-call
mechanics, so that special iteration constructs are useful only as
syntactic sugar.  See section&#xa0;<a class=hrefinternal href="r7rs-Z-H-5.html#TAG:__tex2page_sec_3.5">3.5</a>.</p>
<p>

Scheme procedures are objects in their own right.  Procedures can be
created dynamically, stored in data structures, returned as results of
procedures, and so on.
One distinguishing feature of Scheme is that continuations, which
in most other languages only operate behind the scenes, also have
&#x201c;first-class&#x201d; status.  Continuations are useful for implementing a
wide variety of advanced control constructs, including non-local exits,
backtracking, and coroutines.  See section&#xa0;<a class=hrefinternal href="r7rs-Z-H-8.html#TAG:__tex2page_sec_6.10">6.10</a>.</p>
<p>

Arguments to Scheme procedures are always passed by value, which
means that the actual argument expressions are evaluated before the
procedure gains control, regardless of whether the procedure needs the
result of the evaluation.
Scheme&#x2019;s model of arithmetic is designed to remain as independent as
possible of the particular ways in which numbers are represented within a
computer. In Scheme, every integer is a rational number, every rational is a
real, and every real is a complex number.  Thus the distinction between integer
and real arithmetic, so important to many programming languages, does not
appear in Scheme.  In its place is a distinction between exact arithmetic,
which corresponds to the mathematical ideal, and inexact arithmetic on
approximations.  Exact arithmetic is not limited to integers.</p>
<p>

</p>
<a id="TAG:__tex2page_sec_1.2"></a>
<h2 class=section><a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc_TAG:__tex2page_sec_1.2">1.2&#xa0;&#xa0;Syntax</a></h2>
<p class=noindent>Scheme, like most dialects of Lisp, employs a fully parenthesized prefix
notation for programs and other data; the grammar of Scheme generates a
sublanguage of the language used for data.  An important
consequence of this simple, uniform representation is that
Scheme programs and data can easily be treated uniformly by other Scheme programs.
For example, the <span style="font-family: monospace">eval</span> procedure evaluates a Scheme program expressed
as data.</p>
<p>

The <span style="font-family: monospace">read</span> procedure performs syntactic as well as lexical decomposition of
the data it reads.  The <span style="font-family: monospace">read</span> procedure parses its input as data
(section&#xa0;<a class=hrefinternal href="r7rs-Z-H-9.html#TAG:__tex2page_sec_7.1.2">7.1.2</a>), not as program.</p>
<p>

The formal syntax of Scheme is described in section&#xa0;<a class=hrefinternal href="r7rs-Z-H-9.html#TAG:__tex2page_sec_7.1">7.1</a>.</p>
<p>

</p>
<a id="TAG:__tex2page_sec_1.3"></a>
<h2 class=section><a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc_TAG:__tex2page_sec_1.3">1.3&#xa0;&#xa0;Notation and terminology</a></h2>
<p class=noindent></p>
<a id="TAG:__tex2page_sec_1.3.1"></a>
<h3 class=section><a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc_TAG:__tex2page_sec_1.3.1">1.3.1&#xa0;&#xa0;Base and optional features</a></h3>
<p class=noindent></p>
<p>

Every identifier defined in this report appears in one or more of several
<a id="TAG:__tex2page_index_4"></a><em>libraries</em>.  Identifiers defined in the <a id="TAG:__tex2page_index_6"></a><em>base library</em>
are not marked specially in the body of the report.  
This library includes the core syntax of Scheme
and generally useful procedures that manipulate data.  For example, the
variable <span style="font-family: monospace">abs</span> is bound to a
procedure of one argument that computes the absolute value of a
number, and the variable <span style="font-family: monospace">+</span> is bound to a procedure that computes
sums.  The full list 
all the standard libraries and the identifiers they export is given in
Appendix&#xa0;<a class=hrefinternal href="r7rs-Z-H-10.html#TAG:__tex2page_chap_A">A</a>.</p>
<p>

All implementations of Scheme:
</p>
<ul>
<p>

</p>
<li><p>
Must provide the base library and all the identifiers
exported from it.</p>
<p>

</p>
<li><p>
May provide or omit the other
libraries given in this report, but each library must either be provided
in its entirety, exporting no additional identifiers, or else omitted
altogether.</p>
<p>

</p>
<li><p>
May provide other libraries not described in this report.</p>
<p>

</p>
<li><p>
May also extend the function of any identifier in this
report, provided the extensions are not in conflict with the language
reported here.</p>
<p>

</p>
<li><p>
Must support portable
code by providing a mode of operation in which the lexical syntax does
not conflict with the lexical syntax described in this report.
</p>
</ul>
<p class=noindent>
</p>
<a id="TAG:__tex2page_sec_1.3.2"></a>
<h3 class=section><a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc_TAG:__tex2page_sec_1.3.2">1.3.2&#xa0;&#xa0;Error situations and unspecified behavior</a></h3>
<p class=noindent></p>
<p>

<a id="TAG:__tex2page_index_8"></a>When speaking of an error situation, this report uses the phrase &#x201c;an
error is signaled&#x201d; to indicate that implementations must detect and
report the error.
An error is signaled by raising a non-continuable exception, as if by
the procedure <span style="font-family: monospace">raise</span> as described in section&#xa0;<a class=hrefinternal href="r7rs-Z-H-8.html#TAG:__tex2page_sec_6.11">6.11</a>.  The object raised is implementation-dependent
and need not be distinct from objects previously used for the same purpose.
In addition to errors signaled in situations described in this
report, programmers can signal their own errors and handle signaled errors.</p>
<p>

The phrase &#x201c;an error that satisfies <em>predicate</em> is signaled&#x201d; means that an error is
signaled as above.  Furthermore, if the object that is signaled is
passed to the specified predicate (such as <span style="font-family: monospace">file-error?</span> or <span style="font-family: monospace">
read-error?</span>), the predicate returns <span style="font-family: monospace">#t</span>.</p>
<p>

If such wording does not appear in the discussion of
an error, then implementations are not required to detect or report the
error, though they are encouraged to do so.
Such a situation is sometimes, but not always, referred to with the phrase
&#x201c;an error.&#x201d;
In such a situation, an implementation may or may not signal an error;
if it does signal an error, the object that is signaled may or may not
satisfy the predicates <span style="font-family: monospace">error-object?</span>, <span style="font-family: monospace">file-error?</span>, or
<span style="font-family: monospace">read-error?</span>.
Alternatively, implementations may provide non-portable extensions.</p>
<p>

For example, it is an error for a procedure to be passed an argument of a type that
the procedure is not explicitly specified to handle, even though such
domain errors are seldom mentioned in this report.  Implementations may
signal an error,
extend a procedure&#x2019;s domain of definition to include such arguments,
or fail catastrophically.</p>
<p>

This report uses the phrase &#x201c;may report a violation of an
implementation restriction&#x201d; to indicate circumstances under which an
implementation is permitted to report that it is unable to continue
execution of a correct program because of some restriction imposed by the
implementation.  Implementation restrictions are discouraged,
but implementations are encouraged to report violations of implementation
restrictions.<a id="TAG:__tex2page_index_10"></a></p>
<p>

For example, an implementation may report a violation of an
implementation restriction if it does not have enough storage to run a
program,
or if an arithmetic operation would produce an exact number that is
too large for the implementation to represent.</p>
<p>

If the value of an expression is said to be &#x201c;unspecified,&#x201d; then
the expression must evaluate to some object without signaling an error,
but the value depends on the implementation; this report explicitly does
not say what value is returned. <a id="TAG:__tex2page_index_12"></a></p>
<p>

Finally, the words and phrases &#x201c;must,&#x201d; &#x201c;must not,&#x201d; &#x201c;shall,&#x201d;
&#x201c;shall not,&#x201d; &#x201c;should,&#x201d; &#x201c;should not,&#x201d; &#x201c;may,&#x201d; &#x201c;required,&#x201d;
&#x201c;recommended,&#x201d; and &#x201c;optional,&#x201d; although not capitalized in this
report, are to be interpreted as described in RFC&#xa0;2119&#xa0;[<a class=hrefinternal href="r7rs-Z-H-15.html#TAG:__tex2page_bib_3">3</a>].
They are used only with reference to implementer or implementation behavior,
not with reference to programmer or program behavior.</p>
<p>

</p>
<a id="TAG:__tex2page_sec_1.3.3"></a>
<h3 class=section><a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc_TAG:__tex2page_sec_1.3.3">1.3.3&#xa0;&#xa0;Entry format</a></h3>
<p class=noindent>Chapters&#xa0;<a class=hrefinternal href="r7rs-Z-H-6.html#TAG:__tex2page_chap_4">4</a> and&#xa0;<a class=hrefinternal href="r7rs-Z-H-8.html#TAG:__tex2page_chap_6">6</a> are organized
into entries.  Each entry describes one language feature or a group of
related features, where a feature is either a syntactic construct or a
procedure.  An entry begins with one or more header lines of the form</p>
<p>

</p>

<p class=noindent></p>
<div class=leftline><u><i>category</i>:</u><span style="margin-left: .5em"> </span><span style="font-family: monospace"><i>template</i></span>&#xa0;</div>
<p>

for identifiers in the base library, or</p>
<p>

</p>

<p class=noindent></p>
<div class=leftline><u><i>name</i> library <i>category</i>:</u><span style="margin-left: .5em"> </span><span style="font-family: monospace"><i>template</i></span>&#xa0;</div>
<p>

where <i>name</i> is the short name of a library
as defined in Appendix&#xa0;<a class=hrefinternal href="r7rs-Z-H-10.html#TAG:__tex2page_chap_A">A</a>.</p>
<p>

If <i>category</i> is &#x201c;syntax,&#x201d; the entry describes an expression
type, and the template gives the syntax of the expression type.
Components of expressions are designated by syntactic variables, which
are written using angle brackets, for example &#x3c;expression&#x3e; and
&#x3c;variable&#x3e;.  Syntactic variables are intended to denote segments of
program text; for example, &#x3c;expression&#x3e; stands for any string of
characters which is a syntactically valid expression.  The notation
</p>
<p>

<span style="margin-left: 2em"> </span>&#x3c;thing<sub>1</sub>&#x3e; &#x2026;
</p>
<p>
indicates zero or more occurrences of a &#x3c;thing&#x3e;, and
</p>
<p>

<span style="margin-left: 2em"> </span>&#x3c;thing<sub>1</sub>&#x3e; &#x3c;thing<sub>2</sub>&#x3e; &#x2026;
</p>
<p>
indicates one or more occurrences of a &#x3c;thing&#x3e;.</p>
<p>

If <i>category</i> is &#x201c;auxiliary syntax,&#x201d; then the entry describes a
syntax binding that occurs only as part of specific surrounding
expressions. Any use as an independent syntactic construct or
variable is an error.</p>
<p>

If <i>category</i> is &#x201c;procedure,&#x201d; then the entry describes a procedure, and
the header line gives a template for a call to the procedure.  Argument
names in the template are <i>italicized</i>.  Thus the header line</p>
<p>

</p>

<p class=noindent></p>
<div class=leftline><u>procedure:</u><span style="margin-left: .5em"> </span><span style="font-family: monospace">(vector-ref <i>vector</i> <i>k</i>)</span>&#xa0;</div>
<p>

indicates that the procedure bound to the <span style="font-family: monospace">vector-ref</span> variable takes
two arguments, a vector <i>vector</i> and an exact non-negative integer
<i>k</i> (see below).  The header lines</p>
<p>

</p>

<p class=noindent></p>
<div class=leftline><u>procedure:</u><span style="margin-left: .5em"> </span><span style="font-family: monospace">(make-vector <i>k</i>)</span>&#xa0;</div>

<div class=leftline><u>procedure:</u><span style="margin-left: .5em"> </span><span style="font-family: monospace">(make-vector <i>k</i> <i>fill</i>)</span>&#xa0;</div>
<p>

indicate that the <span style="font-family: monospace">make-vector</span> procedure must be defined to take
either one or two arguments.</p>
<p>


It is an error for a procedure to be presented with an argument that it
is not specified to handle.  For succinctness, we follow the convention
that if an argument name is also the name of a type listed in
section&#xa0;<a class=hrefinternal href="r7rs-Z-H-5.html#TAG:__tex2page_sec_3.2">3.2</a>, then it is an error if that argument is not of the named type.
For example, the header line for <span style="font-family: monospace">vector-ref</span> given above dictates that the
first argument to <span style="font-family: monospace">vector-ref</span> is a vector.  The following naming
conventions also imply type restrictions:

</p>
<div class="mathdisplay leftline"><table style="margin-left:auto; margin-right:auto"><tr><td></td><td><table><tr><td class=centerline>
</td><td><table><tr><td class=centerline>

</td><td><table><tr><td class=centerline><table border=0><tr><td valign=top >
<p class=noindent><em>a</em><em>l</em><em>i</em><em>s</em><em>t</em></td><td valign=top >association list (list of pairs)</td></tr>
<tr><td valign=top ></p>

<p class=noindent><em>b</em><em>o</em><em>o</em><em>l</em><em>e</em><em>a</em><em>n</em></td><td valign=top >boolean value (<span style="font-family: monospace">#t</span> or <span style="font-family: monospace">#f</span>)</td></tr>
<tr><td valign=top ></p>

<p class=noindent><em>b</em><em>y</em><em>t</em><em>e</em></td><td valign=top >exact integer 0 &#x2264;<em>b</em><em>y</em><em>t</em><em>e</em> &#x3c; 256</td></tr>
<tr><td valign=top ></p>

<p class=noindent><em>b</em><em>y</em><em>t</em><em>e</em><em>v</em><em>e</em><em>c</em><em>t</em><em>o</em><em>r</em></td><td valign=top >bytevector</td></tr>
<tr><td valign=top ></p>

<p class=noindent><em>c</em><em>h</em><em>a</em><em>r</em></td><td valign=top >character</td></tr>
<tr><td valign=top ></p>

<p class=noindent><em>e</em><em>n</em><em>d</em></td><td valign=top >exact non-negative integer</td></tr>
<tr><td valign=top ></p>

<p class=noindent><em>k</em>, </p>

<p class=noindent><em>k</em><sub>1</sub>, &#x2026; </p>

<p class=noindent><em>k</em><sub><em>j</em></sub>, &#x2026;</td><td valign=top >exact non-negative integer</td></tr>
<tr><td valign=top ></p>

<p class=noindent><em>l</em><em>e</em><em>t</em><em>t</em><em>e</em><em>r</em></td><td valign=top >alphabetic character</td></tr>
<tr><td valign=top ></p>

<p class=noindent><em>l</em><em>i</em><em>s</em><em>t</em>, </p>

<p class=noindent><em>l</em><em>i</em><em>s</em><em>t</em><sub>1</sub>, &#x2026; </p>

<p class=noindent><em>l</em><em>i</em><em>s</em><em>t</em><sub><em>j</em></sub>, &#x2026;</td><td valign=top >list (see section&#xa0;<a class=hrefinternal href="r7rs-Z-H-8.html#TAG:__tex2page_sec_6.4">6.4</a>)</td></tr>
<tr><td valign=top ></p>

<p class=noindent><em>n</em>, </p>

<p class=noindent><em>n</em><sub>1</sub>, &#x2026; </p>

<p class=noindent><em>n</em><sub><em>j</em></sub>, &#x2026;</td><td valign=top >integer</td></tr>
<tr><td valign=top ><i>obj</i></td><td valign=top >any object</td></tr>
<tr><td valign=top ></p>

<p class=noindent><em>p</em><em>a</em><em>i</em><em>r</em></td><td valign=top >pair</td></tr>
<tr><td valign=top ></p>

<p class=noindent><em>p</em><em>o</em><em>r</em><em>t</em></td><td valign=top >port</td></tr>
<tr><td valign=top ></p>

<p class=noindent><em>p</em><em>r</em><em>o</em><em>c</em></td><td valign=top >procedure</td></tr>
<tr><td valign=top ></p>

<p class=noindent><em>q</em>, </p>

<p class=noindent><em>q</em><sub>1</sub>, &#x2026; </p>

<p class=noindent><em>q</em><sub><em>j</em></sub>, &#x2026;</td><td valign=top >rational number</td></tr>
<tr><td valign=top ></p>

<p class=noindent><em>s</em><em>t</em><em>a</em><em>r</em><em>t</em></td><td valign=top >exact non-negative integer</td></tr>
<tr><td valign=top ></p>

<p class=noindent><em>s</em><em>t</em><em>r</em><em>i</em><em>n</em><em>g</em></td><td valign=top >string</td></tr>
<tr><td valign=top ></p>

<p class=noindent><em>s</em><em>y</em><em>m</em><em>b</em><em>o</em><em>l</em></td><td valign=top >symbol</td></tr>
<tr><td valign=top ></p>

<p class=noindent><em>t</em><em>h</em><em>u</em><em>n</em><em>k</em></td><td valign=top >zero-argument procedure</td></tr>
<tr><td valign=top ></p>

<p class=noindent><em>v</em><em>e</em><em>c</em><em>t</em><em>o</em><em>r</em></td><td valign=top >vector</td></tr>
<tr><td valign=top ></p>

<p class=noindent><em>x</em>, </p>

<p class=noindent><em>x</em><sub>1</sub>, &#x2026; </p>

<p class=noindent><em>x</em><sub><em>j</em></sub>, &#x2026;</td><td valign=top >real number</td></tr>
<tr><td valign=top ></p>

<p class=noindent><em>y</em>, </p>

<p class=noindent><em>y</em><sub>1</sub>, &#x2026; </p>

<p class=noindent><em>y</em><sub><em>j</em></sub>, &#x2026;</td><td valign=top >real number</td></tr>
<tr><td valign=top ></p>

<p class=noindent><em>z</em>, </p>

<p class=noindent><em>z</em><sub>1</sub>, &#x2026; </p>

<p class=noindent><em>z</em><sub><em>j</em></sub>, &#x2026;</td><td valign=top >complex number</td></tr>
<tr><td valign=top ></p>
</td></tr></table></td></tr></table></td><td></td></tr></table></td><td></td></tr></table></td><td></td></tr></table></div>
<p class=noindent></p>
<p>

The names </p>

<p class=noindent><em>s</em><em>t</em><em>a</em><em>r</em><em>t</em> and </p>

<p class=noindent><em>e</em><em>n</em><em>d</em> are used as indexes into strings,
vectors, and bytevectors.  Their use implies the following:</p>
<p>

</p>
<ul>
<p>

</p>
<li><p>
It is an error if <i>start</i> is greater than <i>end</i>.</p>
<p>

</p>
<li><p>
It is an error if <i>end</i> is greater than the length of the
string, vector, or bytevector.</p>
<p>

</p>
<li><p>
If <i>start</i> is omitted, it is assumed to be zero.</p>
<p>

</p>
<li><p>
If <i>end</i> is omitted, it assumed to be the length of the string,
vector, or bytevector.</p>
<p>

</p>
<li><p>
The index <i>start</i> is always inclusive and the index <i>end</i> is always
exclusive.  As an example, consider a string.  If
<i>start</i> and <i>end</i> are the same, an empty
substring is referred to, and if <i>start</i> is zero and <i>end</i> is
the length of <i>string</i>, then the entire string is referred to.</p>
<p>

</p>
</ul>
<p class=noindent>
</p>
<a id="TAG:__tex2page_sec_1.3.4"></a>
<h3 class=section><a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc_TAG:__tex2page_sec_1.3.4">1.3.4&#xa0;&#xa0;Evaluation examples</a></h3>
<p class=noindent>The symbol &#x201c;&#x27f9;&#x201d; used in program examples is read
&#x201c;evaluates to.&#x201d;  For example,</p>
<p>

</p>
<span style="font-family: monospace">(*&#xa0;5&#xa0;8)&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;<span style="margin-left: 2em"> </span>&#x27f9;&#xa0;&#xa0;40</span>
means that the expression <span style="font-family: monospace">(* 5 8)</span> evaluates to the object <span style="font-family: monospace">40</span>.
Or, more precisely:  the expression given by the sequence of characters
&#x201c;<span style="font-family: monospace">(* 5 8)</span>&#x201d; evaluates, in an environment containing the base library, to an object
that can be represented externally by the sequence of characters &#x201c;<span style="font-family: monospace">
40</span>.&#x201d;  See section&#xa0;<a class=hrefinternal href="r7rs-Z-H-5.html#TAG:__tex2page_sec_3.3">3.3</a> for a discussion of external
representations of objects.<p>

</p>
<a id="TAG:__tex2page_sec_1.3.5"></a>
<h3 class=section><a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc_TAG:__tex2page_sec_1.3.5">1.3.5&#xa0;&#xa0;Naming conventions</a></h3>
<p class=noindent>By convention, <span style="font-family: monospace">?</span> is the final character of the names
of procedures that always return a boolean value.
Such procedures are called <a id="TAG:__tex2page_index_14"></a><em>predicates</em>.
Predicates are generally understood to be side-effect free, except that they
may raise an exception when passed the wrong type of argument.</p>
<p>

Similarly, <span style="font-family: monospace">!</span> is the final character of the names
of procedures that store values into previously
allocated locations (see section&#xa0;<a class=hrefinternal href="r7rs-Z-H-5.html#TAG:__tex2page_sec_3.4">3.4</a>).
Such procedures are called <a id="TAG:__tex2page_index_16"></a><em>mutation procedures</em>.
The value returned by a mutation procedure is unspecified.</p>
<p>

By convention, &#x201c;<span style="font-family: monospace">-&#x3e;</span>&#x201d; appears within the names of procedures that
take an object of one type and return an analogous object of another type.
For example, <span style="font-family: monospace">list-&#x3e;vector</span> takes a list and returns a vector whose
elements are the same as those of the list.</p>
<p>

A <a id="TAG:__tex2page_index_18"></a><em>command</em> is a procedure that does not return useful values
to its continuation.</p>
<p>

A <a id="TAG:__tex2page_index_20"></a><em>thunk</em> is a procedure that does not accept arguments.
</p>
<div class=smallskip></div>
<p style="margin-top: 0pt; margin-bottom: 0pt">
</p>
<div class=navigation>[Go to <span><a class=hrefinternal href="r7rs.html">first</a>, <a class=hrefinternal href="r7rs-Z-H-2.html">previous</a></span><span>, <a class=hrefinternal href="r7rs-Z-H-4.html">next</a></span> page<span>; &#xa0;&#xa0;</span><span><a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc">contents</a></span><span><span>; &#xa0;&#xa0;</span><a class=hrefinternal href="r7rs.html#TAG:__tex2page_index_start">index</a></span>]</div>
<p>
</p>
</div>
</body>
</html>

