let
  pkgs = import <nixpkgs> {};
  texmath = pkgs.haskellPackages.texmath;
  texmath-executable = pkgs.haskell.lib.addBuildDepends (
    pkgs.haskell.lib.enableCabalFlag texmath "executable"
  ) [ pkgs.haskellPackages.network-uri ];
  oldPandocPkgs = import (builtins.fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/336eda0d07dc5e2be1f923990ad9fdb6bc8e28e3.tar.gz";
  }) {};
  oldPandoc = oldPandocPkgs.haskellPackages.pandoc;
in
pkgs.mkShell {
  buildInputs = [
    texmath-executable
    pkgs.guile
    oldPandoc
    pkgs.html-tidy
  ];
}
