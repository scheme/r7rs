;; -*- geiser-scheme-implementation: guile -*-
(define-library (r7-fascicle)
  (import (scheme base)
          (scheme file)

          (srfi srfi-1)
          (srfi srfi-13)

          (sxml apply-templates)
          (sxml transform)
          (sxml xpath)

          (pandoc)
          (scheme-doc)
          (scheme-doc html-stylesheet)
          (html-writer))
  (export render-fascicle)

  (begin
    (define (make-r7rs-fascicle-page fascicle-title fascicle-outline page-id page-class page-title internal-draft? content)
      `(*TOP*
        (*doctype* html)
        (html
         (@ (lang "en"))
         (head
          (meta (@ (charset "utf-8")))
          (meta (@ (name "viewport") (content "width=device-width,initial-scale=1")))
          (title ,(string-append fascicle-title ": " page-title))
          (link (@ (rel "stylesheet") (href "style.css"))))

         ,@(if internal-draft?
               '((aside
                  (@ (class "draft-only"))
                  "Working Group internal draft – not final"))
               '())

         (main
          (@ (id ,page-id) (class ,page-class))
          (header
           (h1 ,fascicle-title))
          ,content)

         (nav
          (@ (id "contents"))
          (h2 "Contents")
          ,(outline->table-of-contents fascicle-outline page-id)))))
    
    (define pandoc-render-settings
      '((citeproc . #t)
        (mathml . #t)
        (csl . "render/new-harts-rules-the-oxford-style-guide-author-date.csl")
        (metadata-file . "render/suppress-bibliography.yaml")))

    (define pandoc-render-bibliography-settings
      '((citeproc . #t)
        (csl . "render/new-harts-rules-the-oxford-style-guide-author-date.csl")
        (metadata-file . "render/generate-bibliography.yaml")))

    (define (render-fascicle input-stream bibliography)
      ;; Discard the first line, which contains only Emacs buffer vars
      (read-line input-stream)
      (let* ((html-full
              (combine-html-chunks
               (render-html-chunks input-stream bibliography)))
             (fascicle-title (car ((sxpath '(// h1 *text*)) html-full)))
             (chapters (fascicle-chapters html-full))
             (chapters (number-chapters chapters))
             (chapters (fix-links chapters))
             (outline (chapters->outline chapters)))
        (for-each
         (lambda (chapter)
           (let* ((chapter-id (car ((sxpath '(// h2 @ id *text*))
                                    chapter)))
                  (chapter-title-text (string-join ((sxpath '(// h2 *text*))
                                                    chapter)))
                  (chapter-type
                    ((sxpath '(// h2 @ data-chapter_type *text*))
                     chapter))
                  (of (open-output-file (string-append "html/" chapter-id ".html"))))
             (write-html
              (make-r7rs-fascicle-page fascicle-title
                                       outline
                                       chapter-id
                                       (if (null? chapter-type) "chapter" (car chapter-type))
                                       chapter-title-text
                                       #f
                                       chapter)
              of)
             (close-port of)))
         chapters)

        (let ((bibliography
               (pandoc->sxml "markdown"
                             ""
                             (cons (cons 'bibliography bibliography)
                                   pandoc-render-bibliography-settings)))
              (of (open-output-file (string-append "html/_references.html"))))
          (write-html
              (make-r7rs-fascicle-page fascicle-title
                                       outline
                                       "_bibliography"
                                       "bibliography"
                                       "References"
                                       #t
                                       bibliography)
              of)
          (close-port of))

        chapters))

    (define (fascicle-chapters html)
      (define (not-chapter-start? x)
        (or (not (pair? x))
            (not (eq? (car x) 'h2))))
      (let loop ((more-elts (drop-while not-chapter-start? html))
                 (chapters '()))
        (if (null? more-elts)
            (map (lambda (x) (cons 'div x)) (reverse chapters))
            (let-values (((chapter more)
                          (span not-chapter-start? (cdr more-elts))))
              (loop more
                    (cons (cons (car more-elts) chapter) chapters))))))

    (define alphabet-uppercase "ABCDEFGHIJKLMNOPQRSTUVWXYZ")
    (define (number-chapters chapters)
      (define (number-chapter chapter number)
        (let ((section-number 0))
          (pre-post-order
           chapter
           `((h2
              *preorder*
              . ,(lambda node
                   `(hgroup
                     (@ (style
                            ,(if (char? number)
                                 (string-append "counter-reset: appendix "
                                                (number->string
                                                 (+ 1
                                                    (string-index
                                                     alphabet-uppercase
                                                     number))))
                                 (string-append "counter-reset: chapter "
                                                (number->string number)))))
                     (p ,(if (char? number)
                             (string-append "Appendix " (string number))
                             (string-append "Chapter " (number->string number))))
                     (h2 (@ ,@((sxpath '(@ *)) node)
                            (data-section-number
                             ,(if (char? number) (string number) (number->string number))))
                         ,@(cddr node)))))
             (h3
              *preorder*
              . ,(lambda node
                   (set! section-number (+ section-number 1))
                   `(h3
                     (@ ,@((sxpath '(@ *)) node)
                        (data-section-number
                         ,(string-append
                           (if (char? number) (string number) (number->string number))
                           "."
                           (number->string section-number))))
                     ,@(cddr node))))
             (*text*
              . ,(lambda (_ node) node))
             (*default* . ,(lambda node node))))))
      (let loop ((more chapters)
                 (chapters '())
                 (chapter-number 1)
                 (appendix-number 0))
        (if (null? more) (reverse chapters)
            (let ((chapter-type
                   ((sxpath '(// h2 @ data-chapter_type *text*))
                    (car more))))
              (cond ((null? chapter-type)
                     (loop (cdr more)
                           (cons (number-chapter (car more)
                                                 chapter-number)
                                 chapters)
                           (+ chapter-number 1)
                           appendix-number))
                    ((equal? chapter-type '("appendix"))
                     (loop (cdr more)
                           (cons (number-chapter (car more)
                                                 (string-ref alphabet-uppercase appendix-number))
                                 chapters)
                           chapter-number
                           (+ appendix-number 1)))
                    (else
                     (loop (cdr more)
                           (cons (car more) chapters)
                           chapter-number appendix-number)))))))

    (define (render-html-chunks input-stream bibliography)
      (let loop ((html-chunks '())
                 (current-chunk-lines '())
                 (current-chunk-markup 'org))
        (define (chunk->html)
          (case current-chunk-markup
            ((org)
             (pandoc->sxml "org"
                           (string-join (reverse current-chunk-lines) "\n")
                           (cons (cons 'bibliography bibliography)
                                 pandoc-render-settings)))
            ((scheme-doc)
             (apply-templates (parse-scheme-doc
                               (string-join (reverse current-chunk-lines) "\n"))
                              html-stylesheet))))
        (let ((current-line (read-line input-stream)))
          (cond ((eof-object? current-line)
                 (reverse (cons (chunk->html) html-chunks)))
                ((and (eq? current-chunk-markup 'org)
                      (string=? current-line
                                "@@@ SCHEME DOCUMENTATION STARTS HERE @@@"))
                 (loop (cons (chunk->html) html-chunks) '() 'scheme-doc))
                ((and (eq? current-chunk-markup 'scheme-doc)
                      (string=? current-line
                                "@@@ SCHEME DOCUMENTATION ENDS HERE @@@"))
                 (loop (cons (chunk->html) html-chunks) '() 'org))
                (else (loop html-chunks
                            (cons current-line current-chunk-lines)
                            current-chunk-markup))))))

    (define (combine-html-chunks chunks)
      (cons 'div (append-map cdr chunks)))

    (define (fix-links chapters)
      (let ((xref-dests (chapter-ids chapters)))
        (map
         (lambda (chapter)
           (pre-post-order
            chapter
            `((a
               *preorder*
               . ,(lambda node
                    (let ((attributes ((sxpath '(@ *)) node)))
                      (cond ((member '(role "doc-biblioref") attributes)
                             `(a (@ (href ,(string-append "_references.html"
                                                          (cadr (assq 'href attributes))))
                                    ,@(alist-delete 'href attributes))
                                 ,@(cddr node)))
                            ((char=? (string-ref (cadr (assq 'href attributes)) 0) #\#)
                             (let* ((id (substring (cadr (assq 'href attributes)) 1))
                                    (section (assoc id xref-dests)))
                               `(a (@ (href ,(string-append (cadr section)
                                                            ".html#"
                                                            id))
                                      ,@(alist-delete 'href attributes))
                                   ,(caddr section))))
                            (else node)))))
              (*text*
               . ,(lambda (_ node) node))
              (*default* . ,(lambda node node)))))
         chapters)))

    (define (chapter-ids chapters)
      (let loop ((more chapters)
                 (ids '()))
        (if (null? more) ids
            (let* ((chapter (car more))
                   (chapter-id (car ((sxpath '(// h2 @ id *text*))
                                     chapter)))
                   (chapter-number
                    (let ((maybe-number ((sxpath '(// h2 @ data-section-number *text*))
                                         chapter)))
                      (if (null? maybe-number)
                          "..."
                          (car maybe-number)))))
              (loop (cdr more)
                    (cons (list chapter-id chapter-id chapter-number)
                          (append
                           (map
                            (lambda (section)
                              (list (car ((sxpath '(@ id *text*)) section))
                                    chapter-id
                                    (let ((maybe-section-number
                                           ((sxpath '(@ data-section-number *text*)) section)))
                                      (if (null? maybe-section-number)
                                          "..."
                                          (car maybe-section-number)))))
                            ((sxpath '(// h3)) chapter))
                           ids)))))))

    (define (chapters->outline chapters)
      `(ol
        ,@(map
           (lambda (chapter)
             (let ((chapter-id (car ((sxpath '(// h2 @ id *text*))
                                     chapter)))
                   (chapter-title (cddar ((sxpath '(// h2))
                                          chapter)))
                   (chapter-subelts ((sxpath '(*)) chapter))
                   (chapter-type
                    ((sxpath '(// h2 @ data-chapter_type *text*))
                     chapter)))
               (define (entry->outline-item entry)
                 (let* ((headwords
                         (string-tokenize
                          (car ((sxpath '(// @ data-entry-headwords *text*)) entry))))
                        (id (car ((sxpath '(// @ id *text*)) entry)))
                        (main-headword (car headwords))
                        (sub-headwords (cdr headwords))
                        (outline-contents
                         ;; sub-headword generation is broken, only take the main one for now
                         `((code ,main-headword))
                         #;(if (null? sub-headwords)
                             `((code ,main-headword))
                             `((code ,main-headword)
                               " ("
                               ,@(cdr
                                  (append-map
                                   (lambda (sub-headword)
                                     (list ", "
                                           `(code ,sub-headword)))
                                   sub-headwords))
                               ")"))))
                   `(li (@ (class "entry"))
                     (a (@ (href ,(string-append chapter-id ".html#" id)))
                        ,@outline-contents))))
               (define (h3->subsection-title h3)
                 (let ((title-contents (cddr h3))
                       (id (car ((sxpath '(// @ id *text*)) h3))))
                   `(a (@ (href ,(string-append chapter-id ".html#" id)))
                       ,@title-contents)))
               `(li
                 (@ (class
                      ,(cond ((null? chapter-type)
                              "chapter")
                             ((member (car chapter-type)
                                      '("front-matter" "back-matter"))
                              "unnumbered")
                             (else (car chapter-type))))
                    (data-outline-entry-for ,chapter-id))
                 (a (@ (href ,(string-append chapter-id ".html")))
                    ,@chapter-title)
                 (ol
                  ,@(let loop ((more-elts chapter-subelts)
                               (subsections '())
                               (this-subsection #f)
                               (this-subsection-title #f))
                      (cond ((null? more-elts)
                             (if this-subsection
                                 (reverse
                                  (cons `(li ,this-subsection-title
                                             (ol ,@(reverse this-subsection)))
                                        subsections))
                                 (reverse subsections)))
                            (((node-typeof? 'article) (car more-elts))
                             (if this-subsection
                                 (loop
                                  (cdr more-elts)
                                  subsections
                                  (cons (entry->outline-item (car more-elts))
                                        this-subsection)
                                  this-subsection-title)
                                 (loop
                                  (cdr more-elts)
                                  (cons (entry->outline-item (car more-elts))
                                        subsections)
                                  this-subsection
                                  this-subsection-title)))
                            (((node-typeof? 'h3) (car more-elts))
                             (if this-subsection
                                 (loop
                                  (cdr more-elts)
                                  (cons `(li ,this-subsection-title
                                             (ol ,@(reverse this-subsection)))
                                        subsections)
                                  '()
                                  (h3->subsection-title (car more-elts)))
                                 (loop
                                  (cdr more-elts)
                                  subsections
                                  '()
                                  (h3->subsection-title (car more-elts)))))
                            (else (loop (cdr more-elts)
                                        subsections
                                        this-subsection
                                        this-subsection-title))))))))
           chapters)
        (li (@ (class "unnumbered") (data-outline-entry-for "_bibliography"))
            (a (@ (href "_references.html")) "References"))))
    (define (outline->table-of-contents outline page-id)
      ;; todo
      outline)))
