;; -*- geiser-scheme-implementation: guile -*-
(define-library (pandoc)
  (import (scheme base)
          (srfi srfi-1)
          (ice-9 popen)
          (ice-9 textual-ports)
          (only (guile) waitpid)
          (sxml simple))
  (export pandoc
          pandoc->sxml)

  (begin
    (define (pandoc from-lang to-lang source options)
      (let*-values
          (((command)
            `("pandoc"
              "-f" ,from-lang
              "-t" ,to-lang
              ,@(options->flags options)))
           ((o i pids)
            (pipeline (list command))))
        (write-string source i)
        (close-port i)
        (let ((output (get-string-all o)))
          (close-port o)
          (waitpid (car pids))
          output)))

    (define (pandoc->sxml from-lang source options)
      (root-element
       (xml->sxml
        (string-append
         "<div>"
         (pandoc from-lang "html" source options)
         "</div>"))))
    
    (define (root-element sxml-top)
      (if (and (pair? sxml-top)
               (eq? (car sxml-top) '*TOP*))
          (last sxml-top)
          (error "not an SXML document node" sxml-top)))
    (define (options->flags options)
      (define (option-name->flag-name option-name)
        (string-append "--" (symbol->string option-name)))
      (let loop ((flags '())
                 (more options))
        (if (null? more)
            (reverse flags)
            (let ((option-name (caar more))
                  (option-value (cdar more)))
              (cond ((not (eq? option-value #t))
                     (loop (cons* option-value
                                  (option-name->flag-name option-name)
                                  flags)
                           (cdr more)))
                    (else (loop (cons (option-name->flag-name option-name)
                                      flags)
                                (cdr more))))))))))
