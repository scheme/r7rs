;; -*- geiser-scheme-implementation: guile -*-
(define-library (scheme-doc html-stylesheet)
  (import (scheme base)
          (scheme cxr)
          (srfi srfi-1)
          (srfi srfi-13)
          (srfi srfi-14)
          (sxml apply-templates)
          (sxml simple)
          (sxml xpath)
          (texmath))
  (export html-stylesheet)

  (begin
    (define section-types
      '(("DESCRIPTION" . description-section)

        ("EXAMPLE" . examples-section)
        ("EXAMPLES" . examples-section)
        ("IMPLEMENTATION" . implementation-section)
        ("NOTE" . note-section)
        ("RATIONALE" . rationale-section)
        ("REQUIREMENTS" . requirements-section)
        ("SEMANTICS" . semantics-section)
        ("SYNTAX" . syntax-section)
        ("TODO" . todo-section)))
    (define (gather-sections paragraphs)
      (let loop ((this-section '())
                 (this-section-type
                  (cdar section-types))
                 (sections '())
                 (more paragraphs))
        (cond ((null? more)
               (let ((sections-list
                      (reverse (cons
                                (cons this-section-type
                                      (reverse this-section))
                                sections))))
                 (if (equal? (car sections-list) '(description-section))
                     (cdr sections-list)
                     sections-list)))
              ((eq? (caar more) 'section-beginning-paragraph)
               (loop (list (car more))
                     (cdr (assoc (cadadr (car more)) section-types))
                     (cons (cons this-section-type (reverse this-section))
                           sections)
                     (cdr more)))
              (else
               (loop (cons (car more) this-section)
                     this-section-type
                     sections
                     (cdr more))))))

    (define (gather-sections+entries doc-subforms)
      (let loop ((sections+entries '())
                 (more doc-subforms))
        (cond ((null? more)
               (reverse sections+entries))
              ((eq? (caar more) 'entry)
               (loop (cons (car more) sections+entries)
                     (cdr more)))
              (else
               (let-values (((section-paras new-more)
                             (break (lambda (x) (eq? (car x) 'entry)) more)))
                 (loop (append (reverse (gather-sections section-paras))
                               sections+entries)
                       new-more))))))

    (define (remove-top-level-description-sections sections)
      (remove
       null? ; not sure where the nulls in this procedure come from,
             ; can't work out how to get rid of them
       (let loop ((paragraphs+sections '())
                  (more sections))
         (cond ((null? more)
                (reverse paragraphs+sections))
               ((eq? (caar more) 'description-section)
                (let ((maybe-section-beginning (cadar more)))
                  (if (eq? (car maybe-section-beginning)
                           'section-beginning-paragraph)
                      (loop (append (reverse (cddar more))
                                    (append (cddr maybe-section-beginning)
                                            paragraphs+sections))
                            (cdr more))
                      (loop (append (reverse (cdar more))
                                    paragraphs+sections)
                            (cdr more)))))
               (else (loop (cons (car more) paragraphs+sections)
                           (cdr more)))))))

    (define (entry-headwords entry-node)
      (let ((headwords ((sxpath '(// entry-headword inline-code *text*))
                        entry-node)))
        (delete-duplicates
         (remove string-null?
                 (map (lambda (maybe-headword)
                        (string-trim-both maybe-headword
                                          (string->char-set "() \n")))
                      headwords))
         string=?)))

    (define (headwords->id headwords)
      (let ((main-headword (car headwords)))
        (string-concatenate
         (append
          '("_entry_")
          (map
           (lambda (char)
             (if (char-set-contains?
                  (string->char-set "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-")
                  char)
                 (string char)
                 (string-append "_" (number->string (char->integer char) 16))))
           (string->list main-headword))))))

    (define (section-renderer class)
      (lambda (node)
        `(section
          (@ (class ,class))
          ,@(if (null? (cddadr node))
                `((p (span (@ (class "entry-sec-title"))
                           ,(string-titlecase (cadr (cadadr node)))
                           ":"))
                  ,@(apply-templates (cddr node) html-stylesheet))
                `((p (span (@ (class "entry-sec-title"))
                           ,(string-titlecase (cadr (cadadr node)))
                           ":")
                     " "
                     ,@(apply-templates (cdar (cddadr node))
                                        html-stylesheet))
                  ,@(apply-templates (cddr node) html-stylesheet))))))
    
    (define element-children cdr)

    (define html-stylesheet
      `((*text* . ,values)
        (Scheme-Doc
         . ,(lambda (node)
              `(div ,@(map (lambda (subnode)
                             (apply-templates subnode html-stylesheet))
                           (remove-top-level-description-sections
                            (gather-sections+entries
                             (element-children node)))))))
        (entry
         . ,(lambda (node)
              (let ((headwords (entry-headwords node)))
                `(article
                  (@ (class "entry")
                     (data-entry-headwords ,(string-join
                                             headwords
                                             " "))
                     (id ,(headwords->id headwords)))
                  ,(apply-templates (cadr node)
                                    html-stylesheet)
                  ,@(apply-templates (gather-sections (cdaddr node))
                                     html-stylesheet)))))
        (entry-heading-paragraph
         . ,(lambda (node)
              `(header ,@(apply-templates (element-children node)
                                          html-stylesheet))))
        (entry-heading
         . ,(lambda (node)
              `(dl ,@(apply-templates (element-children node)
                                      html-stylesheet))))
        (entry-headword
         . ,(lambda (node)
              `(dt ,@(apply-templates (element-children node)
                                      html-stylesheet))))
        (entry-description
         . ,(lambda (node)
              `(dd ,@(apply-templates (element-children node)
                                      html-stylesheet))))

        (description-section
         . ,(lambda (node)
              `(section
                (@ (class "description"))
                ,@(cond ((and (eq? (caar (element-children node))
                                   'section-beginning-paragraph)
                              (not (null? (cddar (element-children node)))))
                         (apply-templates (cons (caddr (cadr node))
                                                (cddr node))
                                          html-stylesheet))
                        ((eq? (caar (element-children node))
                              'section-beginning-paragraph)
                         (apply-templates (cddr node)
                                          html-stylesheet))
                        (else (apply-templates (cdr node)
                                               html-stylesheet))))))
        (requirements-section
         . ,(lambda (node)
              `(section
                (@ (class "requirements"))
                ,@(if (and (eq? (caar (element-children node))
                                'section-beginning-paragraph))
                      (apply-templates (cons (caddr (cadr node))
                                             (cddr node))
                                       html-stylesheet)
                      (apply-templates (cdr node)
                                       html-stylesheet)))))

        (examples-section . ,(section-renderer "examples"))
        (implementation-section . ,(section-renderer "implementation"))
        (note-section . ,(section-renderer "note"))
        (rationale-section . ,(section-renderer "rationale"))
        (semantics-section . ,(section-renderer "semantics"))
        (syntax-section . ,(section-renderer "syntax"))
        (todo-section . ,(section-renderer "todo"))
        
        (ordinary-paragraph
         . ,(lambda (node)
              `(p ,@(apply-templates (element-children node)
                                     html-stylesheet))))
        (block-code
         . ,(lambda (node)
              `(pre (code ,@(apply-templates (element-children node)
                                             html-stylesheet)))))

        (list-items
         . ,(lambda (node)
              `(ul ,@(apply-templates (element-children node)
                                      html-stylesheet))))
        (list-item-paragraph
         . ,(lambda (node)
              `(li (p ,@(apply-templates (element-children node)
                                         html-stylesheet)))))

        (indented-paragraph
         . ,(lambda (node)
              `(blockquote (p ,@(apply-templates (element-children node)
                                                 html-stylesheet)))))

        (mathematic
         . ,(lambda (node)
              (texmath->sxml #t (cadr node))))
        (mathematic-paragraph
         . ,(lambda (node)
              (texmath->sxml #f (cadadr node))))

        (inline-code
         . ,(lambda (node)
              `(code ,@(apply-templates (element-children node)
                                        html-stylesheet))))
        (bare-variable
         . ,(lambda (node)
              `(var ,(string-downcase (cadr node))
                    ,@(apply-templates (cddr node)
                                       html-stylesheet))))
        (syntax-symbol
         . ,(lambda (node)
              `(var
                (@ (class "stx"))
                ,@(apply-templates (element-children node)
                                   html-stylesheet))))
        (subscript
         . ,(lambda (node)
              `(sub ,@(apply-templates (element-children node)
                                       html-stylesheet))))
        (superscript
         . ,(lambda (node)
              `(sub ,@(apply-templates (element-children node)
                                       html-stylesheet))))

        (explicit-linebreak
         . ,(lambda (node) '(br)))

        (editorial-comment
         . ,(lambda (node)
              `(small "[" (i "Editorial note:")
                      ,@(apply-templates (element-children node)
                                         html-stylesheet)
                      "]")))

        (cross-reference
         . ,(lambda (node)
              `(a (@ (href ,(string-append "#"
                                           (car (element-children node))))))))

        (example-paragraph
         . ,(lambda (node)
              `(div
                (@ (class "example"))
                (div
                 (@ (class "example-input"))
                 ,@(apply-templates (cadr node) html-stylesheet))
                (span
                 (@ (class "example-arrow"))
                 ,(cond ((string=? (cadr (caddr node)) "=>") "⇒")
                        ((string=? (cadr (caddr node)) "===") "≡")))
                (div
                 (@ (class "example-output"))
                 ,@(apply-templates (cdddr node) html-stylesheet)))))))))
