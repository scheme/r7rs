;; -*- geiser-scheme-implementation: guile -*-
(define-library (texmath)
  (import (scheme base)
          (ice-9 popen)
          (ice-9 textual-ports)
          (only (guile) waitpid)
          (srfi srfi-1)
          (sxml simple))
  (export texmath->sxml)
  (begin
    (define (invoke-texmath inline? input)
      (let*-values
          (((command)
            (if inline?
                '("texmath" "--inline")
                '("texmath")))
           ((o i pids)
            (pipeline (list command))))
        (write-string input i)
        (close-port i)
        (let ((output (get-string-all o)))
          (close-port o)
          (waitpid (car pids))
          output)))

    (define (root-element sxml-top)
      (if (and (pair? sxml-top)
               (eq? (car sxml-top) '*TOP*))
          (last sxml-top)
          (error "not an SXML document node" sxml-top)))
    
    (define (texmath->sxml inline? input)
      (let ((mathml-source (invoke-texmath inline? input)))
        (root-element (xml->sxml mathml-source))))))
