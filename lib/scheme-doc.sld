;; -*- geiser-scheme-implementation: guile -*-
(define-library (scheme-doc)
  (import (scheme base)
          (srfi srfi-1)
          (srfi srfi-14)
          (ice-9 peg))
  (export parse-scheme-doc)

  (begin
    ;;;; Character sets as PEGs
    (define (char-set->peg cs)
      (lambda (str len pos)
        (and (<= (+ pos 1) len)
             (char-set-contains? cs (string-ref str pos))
             (list (+ pos 1) (list (substring str pos (+ pos 1)))))))

    (define alphabetic (char-set->peg char-set:letter))
    (define upper-case (char-set->peg char-set:upper-case))
    (define digit (char-set->peg char-set:digit))
    (define space (char-set->peg char-set:whitespace))
    (define horizontal-space (char-set->peg char-set:blank))

    (define char-set:non-id-punct (string->char-set "()[]{}'`\";,@_^"))
    (define char-set:id-punct (char-set-difference char-set:punctuation char-set:non-id-punct))

    (define non-id-punct (char-set->peg char-set:non-id-punct))
    (define id-punct (char-set->peg char-set:id-punct))

    (define-peg-pattern linebreak body
      (or "\r\n"
          (and "\r" (not-followed-by "\n"))
          "\n"))
    (define (end-of-input str len pos)
      (and (>= pos len) (list pos '())))

    ;;;; Formal grammar in terms of (ice-9 peg)

    (define-peg-pattern Scheme-Doc all
      (and (ignore (* space))
           (+ (or entry paragraph))))

    (define-peg-pattern paragraph body
      (and (not-followed-by (and "******" linebreak))
           (or section-beginning-paragraph
               example-paragraph
               block-code-paragraph
               list-items
               indented-paragraph
               mathematic-paragraph
               ordinary-paragraph)))

    (define-peg-pattern paragraph-end none
      (or (and linebreak (+ linebreak))
          (and linebreak end-of-input)
          end-of-input))

    (define-peg-pattern entry all
      (and (ignore (and "******" linebreak))
           entry-heading-paragraph
           entry-contents
           (or end-of-input
               (followed-by (and "******" linebreak entry-heading-paragraph))
               (ignore (and "******" linebreak)))))

    (define-peg-pattern entry-contents all
      (+ paragraph))
    
    (define-peg-pattern entry-heading-paragraph all
      (and (+ entry-heading) paragraph-end))

    (define-peg-pattern section-beginning-paragraph all
      (and section-keyword
           (ignore (and ":" (* horizontal-space)))
           (or paragraph-end ordinary-paragraph)))
    (define-peg-pattern section-keyword all
      (and upper-case
           (+ (or upper-case
                  digit
                  "-" "_"))))

    (define-peg-pattern list-items all
      (+ list-item-paragraph))
    (define-peg-pattern list-item-paragraph all
      (and (ignore (and (or "-" "•") (+ horizontal-space)))
           paragraph-contents
           paragraph-end))

    (define-peg-pattern example-paragraph all
      (and example-input
           (ignore (or linebreak
                       (* horizontal-space)))
           example-arrow
           (ignore (or linebreak
                       (* horizontal-space)))
           example-output paragraph-end))
    (define-peg-pattern example-arrow all
      (or "=>" "==="))
    (define-peg-pattern example-output all
      (and (? block-code)
           paragraph-contents))
    (define-peg-pattern indented-paragraph all
      (and (ignore (and ">" (+ horizontal-space)))
           paragraph-contents
           paragraph-end))
    (define-peg-pattern ordinary-paragraph all
      (and paragraph-contents paragraph-end))

    (define-peg-pattern entry-heading all
      (and entry-headword
           (ignore (and (or (+ horizontal-space) (? linebreak))
                        "---"
                        (or (+ horizontal-space) (? linebreak))))
           entry-description
           (? (and linebreak (not-followed-by linebreak)))))
    (define-peg-pattern entry-headword all
      inline-code)
    (define-peg-pattern entry-description all
      (* (or verbatim inline-code syntax-symbol
             (or (range #\null #\tab)
                 (and linebreak (not-followed-by (or linebreak entry-heading)))
                 (range #\x11 #\x10FFFF)))))

    (define-peg-pattern paragraph-contents body
      (* (or editorial-comment cross-reference
             verbatim inline-code syntax-symbol mathematic
             explicit-linebreak
             (or (range #\null #\tab)
                 (and linebreak (not-followed-by linebreak))
                 (range #\x11 #\x10FFFF)))))

    (define-peg-pattern explicit-linebreak all
      (and (ignore "\\\\") linebreak))

    (define-peg-pattern editorial-comment all
      (and (ignore "(@@")
           (+ (or verbatim inline-code syntax-symbol mathematic
                  (range #\null #\x3f)
                  (and "@" (not-followed-by "@)"))
                  (and "@@" (not-followed-by ")"))
                  (range #\x41 #\x10FFFF)))
           (ignore "@@)")))

    (define-peg-pattern cross-reference all
      (and (ignore "->{")
           (+ (or alphabetic id-punct))
           (ignore "}")))

    (define-peg-pattern syntax-symbol all
      (and (ignore "<<")
           syntax-symbol-name
           (? sub-super-script)
           (ignore ">>")))
    (define-peg-pattern syntax-symbol-name body
      (+ (or verbatim
             (range #\null #\tab)
             (and linebreak (not-followed-by linebreak))
             (range #\x11 #\x3D)
             (and ">" (not-followed-by ">"))
             (range #\x3F #\x5D)
             (range #\x60 #\x10FFFF))))

    (define-peg-pattern inline-code all
      (or double-quoted-inline-code single-quoted-inline-code))
    (define-peg-pattern example-input all
      (or block-code inline-code))

    (define-peg-pattern single-quoted-inline-code body
      (and (ignore "`")
           (* (or inline-code-markup
                  (range #\null #\tab)
                  (and linebreak (not-followed-by linebreak))
                  (range #\x11 #\x26)
                  (range #\x28 #\x10FFFF)))
           (ignore "'")))
    (define-peg-pattern double-quoted-inline-code body
      (and (ignore "``")
           (* (or inline-code-markup
                  (range #\null #\tab)
                  (and linebreak (not-followed-by linebreak))
                  (range #\x11 #\x26)
                  (and "'" (not-followed-by "'"))
                  (range #\x28 #\x10FFFF)))
           (ignore "''")))

    (define-peg-pattern block-code-paragraph body
      (and block-code paragraph-end))
    (define-peg-pattern block-code all
      (and (ignore (and "```" (? linebreak)))
           (* (or inline-code-markup
                  (range #\null #\tab)
                  (and (+ linebreak) (not-followed-by "'''"))
                  (range #\x11 #\x26)
                  (and "'" (not-followed-by "''"))
                  (and "''" (not-followed-by "'"))
                  (range #\x28 #\x10FFFF)))
           (ignore (and (? linebreak) "'''"))))

    (define (%bare-variable-lookbehind-cheat str len pos)
      ;; necessary to get (ice-9 peg) to give us the contents of
      ;; inline-code as a string and not a list of 1-strings; the pure-PEG
      ;; expression is (non-id-punct / space) variable-name
      (and (> pos 0)
           (or (char-set-contains? char-set:non-id-punct
                                   (string-ref str (- pos 1)))
               (char-set-contains? char-set:whitespace
                                   (string-ref str (- pos 1))))
           (list pos '())))

    (define-peg-pattern inline-code-markup body
      (or verbatim
          (and %bare-variable-lookbehind-cheat bare-variable)
          mathematic
          syntax-symbol))

    (define-peg-pattern bare-variable all
      (and upper-case
           (* (or upper-case
                  digit
                  id-punct))
           (? sub-super-script)
           (followed-by (or space non-id-punct))))

    (define-peg-pattern verbatim body
      (and (ignore "|||")
           (+ (or (range #\null #\x7B)
                  (and "|" (not-followed-by "||"))
                  (and "||" (not-followed-by "|"))
                  (range #\x7D #\x10FFFF)))
           (ignore "|||")))

    (define-peg-pattern sub-super-script body
      (and (? subscript)
           (? superscript)))

    (define-peg-pattern subscript all
      (and (ignore "_") sub-super-script-text))
    (define-peg-pattern superscript all
      (and (ignore "^") sub-super-script-text))

    (define-peg-pattern sub-super-script-text body
      (+ (or alphabetic
             digit
             "-")))

    (define-peg-pattern mathematic-paragraph all
      (and mathematic paragraph-end))

    (define-peg-pattern mathematic all
      (and (ignore "$$")
           (+ (or (range #\null #\x23)
                  (and "$" (not-followed-by "$"))
                  (and (range #\x25 #\x10FFFF))))
           (ignore "$$")))

    ;;;; Utility procedures (ice-9 peg) doesn’t provide

    (define (match-pattern-completely nonterm string)
      (let ((result (match-pattern nonterm string)))
        (if (and result
                 (= (peg:start result) 0)
                 (= (peg:end result) (string-length string)))
            result
            (error "couldn’t parse any further" (peg:end result)))))

    (define (parse-scheme-doc string)
      (peg:tree (match-pattern-completely Scheme-Doc string)))))
