#!/bin/bash

### Build script for R7RS-Large documents.
### Vincent Manis <vmanis AT telus DOT NET>

### Syntax:
###
###   ./build.sh [--setup] directory jobname
###
### Typesets the file directory/jobname.tex into PDF and HTML, leaving
### the result in release/jobname.pdf and the directory
### release/jobname-html; the index page of the latter will be
### release/jobname-html/jobname.html. All the work is done in the
### directory output (to avoid cluttering up the original directory
### with innumerable auxiliary files). At present, two tex and two
### tex2page passes are made. Because this may not be enough, and the
### only way you can really tell is by human inspection, the setup
### phase is optional, so that you can mke two more passes, if you
### need to.

### Grab the parameters.
setup=no
if [ "$1" == "--setup" ]; then
  setup=yes
  shift 1
fi
# I should check that sufficient parameters were given!
directory=$1
jobname=$2

### Which TeX engine is to be used.
tex=pdflatex

function setup {
  ## Make sure output directory exists and is empty.
  rm -rf output
  mkdir output

  ## Later on, I'll move the format files (the class file,
  ## commands.tex, etc) into a separate directory. This will mean that
  ## subsequent reports can use the same files.

  ## Add in the tex2page specification file, and the style file.
  cp r7rs-t2p-macros/r7rs.t2p  output
  cp tex2page/tex2page.{sty,tex} output

  ## Now copy in all the files from the source directory.
  ## The -r flag is almost certainly unnecessary.
  cp -r ${directory}/* output
}

if [ "$setup" == "yes" ]; then
  setup
fi

### Now build the pdf and the html. Make 2 passes for each; you can
### look at the console output to see if further passes are needed.
### The source directory name will become a parameter; that will get
### rid of all the ugly “../..”s.
cd output
  ## Build the PDF. I'll fiddle this later to manage the indexing.
  ${tex} ${jobname}.tex
  ${tex} ${jobname}.tex
  texretcode=$?
  ## Now build html. Unfortunately, tex2page takes the output location
  ## by reading a file with type hdir in the current directory. 
  echo "${jobname}-html" >${jobname}.hdir
  tex2page ${jobname}
  tex2page ${jobname}
  t2pretcode=$?
  ## Both TeX and tex2page produce voluminous output, so display the
  ## summary each produces to see that it works. I don't know if
  ## TeX2Page's exit code is significant, but why not.
  echo "*** TeX results (exit code = ${texretcode}):"
  tail -n10 ${jobname}.log 
  echo "*** TeX2page results (exit code $t2pretcode):"
  tail -n10 ${jobname}-html/${jobname}.hlog
cd ..
  
### Now extract the payload.
cp -r output/${jobname}{.pdf,-html} release
